# Monsoon 2.0 - Global storm resolving simulations

This is the run directory for the "Monsoon 2.0" project. The core project
consists of various storm-resolving (5km) simulations. Each ensemble is forced with
3-hourly sea-surface temperatures (SST) and sea-ice concentrations (SIC)
boundary conditionsfrom different CMIP6 projections.

## Simulations

The run directories are created automatically using a Python script and Jinja 2
templates.

You can either create the run directory for a specific simulation

```sh
scripts/generate_rundirs.py luk1001
```

or create every run directory for the "Monsoon 2.0 ensemble"

```sh
scripts/generate_rundirs.py
```

## Naming scheme

ICON naming tags follow a somewhat arbitrary convention. The actual names are
not used in any meaningful way but we will stick to them anyhow.

Every experiment identifier is build in the following structure:

    tagIJKL

Here, `tag` is a three-digit experimenteer ID, that allows you to guess which
scientists performed the run. You can verify your guess by looking at the
[list of IDs](https://wiki.mpimet.mpg.de/doku.php?id=models:list_of_ids).
For Monsoon 2.0, every simulations will start with the `luk` prefix.

In addition, there is a four-digit number `IJKL`. In theory, this number is
supposed to consecutively increase for every simulation an individual scientist
performs during their career. However, we decided to encode some information
about the simulation into the experiment ID.

The first digit will tell you the resolution of the ICON simulation.

I | Resolution
--|-----------
0 | R2B04
1 | R2B09

When debugging ICON it is often helpful to run in coarser resolution.
Therefore, you can create R2B4 (~160km) run scripts by replacing the `luk10`
prefix with `luk00`. So `luk0013` is the low resolution version of `luk1013`.

The next digit stores information about the conofigration of the microphysics scheme:

J | Microphysics
--|--------------------------------------
0 | Default
1 | Increased fall-speed of ice particles

The last digit stores information about the underlying climate scenario:

K | Background scenario
--|--------------------
0 | "piControl"
1 | "ssp245"
2 | "ssp585"
3 | "piControl-p1K"

In addition, the last digit `L` will tell you the ensemble member (0-based indexing).

In conclusion, the experiment ID `luk1024` is the fifth ensemble member in a
set of ICON simulation in R2B09 resolution (5km) using the default microphysics
settings and an RCP 8.5 background climate.

## Output

Due to changes in the modelling strategy some output variables are not
available for every ensemble member:

1. The first ensemble member (`tagXXX0`) only provide 2D fields (30min).
2. From the second esemble members onwards, some 3D fields are provided as
   weekly means to capture the large-scale atmospheric condition.
3. Starting with the fourth ensemble member, we changed the precipitation
   output; the variable set `(pr, prlr, prls)` are replaced by
   `(rain_gsp_rate, snow_gsp_rate, graupel_gsp_rate)`.
   Data users can retrieve the old variables using the following scheme:

   ```
   prlr = rain_gsp_ratee
   prls = snow_gsp_rate + graupel_gsp_rate
   pr = prlr + prls
   ```
4. Starting with the fifth ensemble member, the 3D fiels are written every 3H.
