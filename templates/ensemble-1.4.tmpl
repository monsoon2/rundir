#! /bin/ksh
#_______________________________________________________________________________________________
#
#SBATCH --account=highresmonsoon
#SBATCH --job-name=exp.{{ experiment_id }}.run
#SBATCH --output=/p/project/highresmonsoon/run/{{ experiment_id }}/LOG.%x.%j.o
#SBATCH --error=/p/project/highresmonsoon/run/{{ experiment_id }}/LOG.%x.%j.o
#SBATCH --nodes=77  # 72 + ceil(io_taks / 4)
#SBATCH --partition=booster
#SBATCH --constraint=gpu
#SBATCH --gres=gpu:4
#SBATCH --exclude=jwb0886
#SBATCH --time=24:00:00
#SBATCH --parsable
#
#_______________________________________________________________________________________________
#
# Load modules
#
MODULES='Stages/2020 NVHPC/21.5-GCC-10.3.0 OpenMPI netCDF-Fortran Java Python'

ml --force purge
ml $MODULES

ml list
#_______________________________________________________________________________________________
#
set -x

ulimit -s unlimited
ulimit -c 0

ln -fs "/p/project/highresmonsoon/run/{{ experiment_id }}/LOG.${SLURM_JOB_NAME}.${SLURM_JOB_ID}.o" LOG.latest.o

#_______________________________________________________________________________________________
#
# ICON run script:
#
#_______________________________________________________________________________________________
#
# MPI related variable handling
#
tasks_per_node=4
io_tasks=18

export SLURM_HOSTFILE=$(pwd -P)/hostfile

./create-hostfile.py --tasks-per-node $tasks_per_node --io-tasks $io_tasks > $SLURM_HOSTFILE

mpi_total_procs=$(wc -l $SLURM_HOSTFILE | awk '{print $1}')
#_______________________________________________________________________________________________
#
# blocking length
#
nproma=75108
rrtmgp_chunk=4000
#
#_______________________________________________________________________________________________
#
# environment variables for the experiment and the target system
#
export EXPNAME="{{ experiment_id }}"

icondir=/p/project/highresmonsoon/ICON
export PATH=$icondir/local.gcc/bin:$icondir/bin:$PATH
export LD_LIBRARY_PATH=$icondir/lib64:$LD_LIBRARY_PATH
export ECCODES_DEFINITION_PATH=/p/project/pra119/eccodes/definitions.mpim:/p/project/pra119/eccodes/defninitions
#_______________________________________________________________________________________________

# directories with absolute paths
# -------------------------------
thisdir=$(pwd -P)
basedir="{{ icon_basedir }}"

icon_data_rootFolder="/p/fastdata/slmet/slmet111/model_data/ICON"

if [[ ! -e $thisdir/run_wrapper.sh ]]
then
    echo launch script $thisdir/run_wrapper.sh not available ...
    exit 1
fi

# how to start the icon model
# ---------------------------
export NVCOMPILER_ACC_SYNCHRONOUS=1
export UCX_RC_TIMEOUT=5s
# export NVCOMPILER_ACC_NOTIFY=1

export START="srun --label -n $mpi_total_procs --cpu-bind none $thisdir/run_wrapper.sh -n $tasks_per_node -o $io_tasks -e "
export MODEL="${basedir}/bin/icon"

# how to submit the next job
# --------------------------
submit="sbatch"
job_name="exp.${EXPNAME}.run"

# define script functions used in the experiment run script
# ---------------------------------------------------------
source ./add_run_routines

#_______________________________________________________________________________________________

#--------------------------------------------------------------------------------------------------
#
# QUBICC experiment hc12
#
author_list="Lukas Kluft and Luis Kornblueh, MPIM"
#
# Setup on Daint:
# - 77 nodes = 72 comp. nodes + 5 output nodes
# - 1 task/node
# - nproma       75108
# - rrtmgp_chunk  4000
#
#--------------------------------------------------------------------------------------------------
#
# This file describes the experiment based on the non-hydrostatic atmosphere and the
# Sapphire physics. The experiment is intialized from IFS analysis files and uses transient
# boundary conditions for:
# - SST and sea ice
# - spectral solar irradiation
# - well mixed greenhouse gases CO2, CH4, N2O, CFCs
# - O3 concentration
#
# Aerosols are neglected for now
#
#--------------------------------------------------------------------------------------------------

# (1) Variables provided by the scripting mechanism

# EXPNAME                       = name of exp. in 'exp.<name>'
# basedir                       = base directory of the ICON repository, where bin/ and run/ exist
# icon_data_rootFolder          = root directory for ICON grids, initial and boundary conditions. 
# nproma                        = blocking length for array dimensioning and inner loop lengths

#--------------------------------------------------------------------------------------------------

# (2) Set variables

# horizontal grid for global domain
atm_grid_id=0015
atm_grid_refinement=R02B09
atm_grid_label=G
atm_grid_file=icon_grid_${atm_grid_id}_${atm_grid_refinement}_${atm_grid_label}.nc

# start and end date+time
start_date="{{ start_date | default("2020-04-01T00:00:00Z") }}"
  end_date="{{ end_date | default("2020-10-01T00:00:00Z") }}"

# restart intervals
checkpoint_interval="P1M"
   restart_interval="P3M"

# output intervals
atm_2d_output_interval="PT30M"
atm_2d_file_interval="P1D"
atm_3d_output_interval="{{ atm_3d_output_interval | default('P3H') }}"
atm_3d_file_interval="P1D"
output_start="$start_date"
output_end="$end_date"

# namelist files
atmo_namelist=NAMELIST_${EXPNAME}_atm
 lnd_namelist=NAMELIST_${EXPNAME}_lnd

# JSBACH variables
run_jsbach=yes
jsbach_usecase=jsbach_lite    # jsbach_lite or jsbach_pfts
jsbach_with_lakes=yes
jsbach_with_hd=no
jsbach_with_carbon=no         # yes needs jsbach_pfts usecase
jsbach_check_wbal=no          # check water balance

# surface variables
ljsbach=$([ "${run_jsbach:=no}" == yes ] && echo .TRUE. || echo .FALSE. )
llake=$([ "${jsbach_with_lakes:=yes}" == yes ] && echo .TRUE. || echo .FALSE. )
lcarbon=$([ "${jsbach_with_carbon:=yes}" == yes ] && echo .TRUE. || echo .FALSE. )
#
if [[ $jsbach_usecase == *pfts* ]]
then
  pft_file_tag="11pfts_"
else
  pft_file_tag=""
fi

#--------------------------------------------------------------------------------------------------

# (3) Define the model configuration

# atmospheric dynamics and physics
# --------------------------------

htop_cloudy=22500. # [m] top height for processing cloud condensates

cat > ${atmo_namelist} << EOF
!
&gribout_nml
 preset                          = 'none'
 generatingCenter                = 252    ! This is MPI-M
 generatingSubcenter             =   3    ! 1 (we at DKRZ), 2 (we at CSCS), 3 (we at JSC) 
 tablesVersion                   =   4    ! WMO predefined table version
 localTablesVersion              =   1    ! MPI-M defined local table version 
 significanceOfReferenceTime     =   2    ! grib2/tables/4/1.2.table
 productionStatusOfProcessedData =   2    ! grib2/tables/4/1.3.table
 typeOfProcessedData             =   1    ! grib2/tables/4/1.4.table
 typeOfGeneratingProcess         =   2    ! grib2/tables/4/4.3.table
 generatingProcessIdentifier     =   0
 lgribout_compress_ccsds         = .FALSE.
/
!
&parallel_nml
 nproma             = ${nproma}
 num_io_procs       = ${io_tasks}
 io_proc_chunk_size = 16
 iorder_sendrecv    = 3
/
&grid_nml
 dynamics_grid_filename = "${atm_grid_file}",
/
&run_nml
 num_lev          = 90         ! number of full levels
 modelTimeStep    = "PT40S"
 ltestcase        = .FALSE.     ! run testcase
 ldynamics        = .TRUE.      ! dynamics
 ltransport       = .TRUE.      ! transport
 iforcing         = 2           ! 0: none, 1: HS, 2: ECHAM, 3: NWP
 output           = 'nml'
 msg_level        = 5           ! level of details report during integration 
 restart_filename = "${EXPNAME}_restart_atm_<rsttime>.mfr"
 activate_sync_timers = .TRUE.
/
&extpar_nml
 itopo            = 1           ! 1: read topography from the grid file
 itype_lwemiss    = 0
/
&initicon_nml
 init_mode        = 2           ! 2: initialize from IFS analysis
 ifs2icon_filename= "ifs2icon.nc"
/
&nonhydrostatic_nml
 ndyn_substeps    = 5           ! dtime/dt_dyn
 damp_height      = 50000.      ! [m]
 rayleigh_coeff   = 1.0
 vwind_offctr     = 0.2
 lhdiff_rcf       = .TRUE.
 divdamp_order    = 24
 divdamp_type     = 3
 divdamp_fac      = 0.004
 thslp_zdiffu     = 0.02
 thhgtd_zdiffu    = 125.
 htop_moist_proc  = ${htop_cloudy}
 hbot_qvsubstep   = 10000.
/
&sleve_nml
 min_lay_thckn    = 40.         ! [m]
 top_height       = 83000.      ! [m]
 stretch_fac      = 0.9
 decay_scale_1    = 4000.       ! [m]
 decay_scale_2    = 2500.       ! [m]
 decay_exp        = 1.2
 flat_height      = ${htop_cloudy}
/
&diffusion_nml
 hdiff_order      = 5
 hdiff_efdt_ratio = 24.0        ! change from default 36.0 to 24.0
 hdiff_smag_fac   = 0.025       ! change from default 0.015 to 0.025
/
&transport_nml
 tracer_names     = 'hus','clw','cli', 'qr', 'qs', 'qg'
 ivadv_tracer     =    3 ,   3 ,   3 ,   3 ,   3 ,   3
 itype_hlimit     =    3 ,   3 ,   3 ,   4 ,   4 ,   4
 ihadv_tracer     =   22 ,  22 ,  22 ,   2 ,   2 ,   2
/
&echam_phy_nml
!
! domain 1
! --------
!
! atmospheric physics (""=off)
 echam_phy_config(1)%dt_rad = "PT12M"
 echam_phy_config(1)%dt_vdf = "PT40S"
 echam_phy_config(1)%dt_mig = "PT40S"
 echam_phy_config(1)%dt_sso = "PT40S"
!
! surface (.TRUE. or .FALSE.)
 echam_phy_config(1)%ljsb       = ${ljsbach}
 echam_phy_config(1)%lamip      = .TRUE.
 echam_phy_config(1)%lice       = .TRUE.
 echam_phy_config(1)%lmlo       = .FALSE.
 echam_phy_config(1)%llake      = ${llake}
 echam_phy_config(1)%lsstice    = .TRUE.
!
! top height for cloud processes
 echam_phy_config(1)%zmaxcloudy = ${htop_cloudy}
!
! fix negative humidity
 echam_phy_config(1)%iqneg_d2p  = 2     ! after dynamics: set neg. humidity to 0
 echam_phy_config(1)%iqneg_p2d  = 2     ! after physics : set neg. humidity to 0
/
&echam_cov_nml
 echam_cov_config(1)%icov       = 3     ! 0/1 scheme based on cloud condensate
 echam_cov_config(1)%cqx        = 1.e-6
/
&echam_cop_nml
 echam_cop_config(1)%cn1lnd     =  50.0
 echam_cop_config(1)%cn2lnd     = 220.0
 echam_cop_config(1)%cn1sea     =  50.0
 echam_cop_config(1)%cn2sea     = 100.0
 echam_cop_config(1)%cinhomi    =   1.0
 echam_cop_config(1)%cinhoml1   =   1.0
 echam_cop_config(1)%cinhoml2   =   1.0
 echam_cop_config(1)%cinhoml3   =   1.0
/
&echam_rad_nml
!
! domain 1
! --------
!
 echam_rad_config(1)%isolrad    =  1
 echam_rad_config(1)%irad_h2o   =  1
 echam_rad_config(1)%irad_co2   =  3
 echam_rad_config(1)%irad_ch4   =  13
 echam_rad_config(1)%irad_n2o   =  13
 echam_rad_config(1)%irad_o3    =  5
 echam_rad_config(1)%irad_o2    =  2
 echam_rad_config(1)%irad_cfc11 =  3
 echam_rad_config(1)%irad_cfc12 =  3
 echam_rad_config(1)%irad_aero  =  0
 echam_rad_config(1)%rrtmgp_columns_chunk = $rrtmgp_chunk
/
&echam_vdf_nml
 echam_vdf_config(1)%pr0        =  0.7
 echam_vdf_config(1)%lmix_max   =  150.0
/
&echam_mig_nml
 echam_mig_config(1)%mu_rain        = 0.5
 echam_mig_config(1)%rain_n0_factor = 0.1
 echam_mig_config(1)%v0snow         = 25.
 echam_mig_config(1)%zvz0i          = {{ zvz0i | default(1.25) }}
 echam_mig_config(1)%icesedi_exp    = {{ icesedi_exp | default(0.33) }}
/
&sea_ice_nml
/
EOF

# land surface and soil
# ---------------------
cat > ${lnd_namelist} << EOF
&jsb_model_nml
  usecase         = "${jsbach_usecase}"
  use_lakes       = ${llake}
  fract_filename  = 'bc_land_frac.nc'
/
&jsb_seb_nml
  bc_filename     = 'bc_land_phys.nc'
  ic_filename     = 'ic_land_soil.nc'
/
&jsb_rad_nml
  use_alb_veg_simple = .TRUE.           ! Use TRUE for jsbach_lite, FALSE for jsbach_pfts
  bc_filename     = 'bc_land_phys.nc'
  ic_filename     = 'ic_land_soil.nc'
/
&jsb_turb_nml
  bc_filename     = 'bc_land_phys.nc'
  ic_filename     = 'ic_land_soil.nc'
/
&jsb_sse_nml
  l_heat_cap_map  = .FALSE.
  l_heat_cond_map = .FALSE.
  l_heat_cap_dyn  = .FALSE.
  l_heat_cond_dyn = .FALSE.
  l_snow          = .TRUE.
  l_dynsnow       = .TRUE.
  l_freeze        = .TRUE.
  l_supercool     = .TRUE.
  bc_filename     = 'bc_land_soil.nc'
  ic_filename     = 'ic_land_soil.nc'
/
&jsb_hydro_nml
  l_organic       = .FALSE.
  bc_filename     = 'bc_land_soil.nc'
  ic_filename     = 'ic_land_soil.nc'
  bc_sso_filename = 'bc_land_sso.nc'
/
&jsb_assimi_nml
  active          = .FALSE.             ! Use FALSE for jsbach_lite, TRUE for jsbach_pfts
/
&jsb_pheno_nml
  scheme          = 'climatology'       ! scheme = logrop / climatology; use climatology for jsbach_lite
  bc_filename     = 'bc_land_phys.nc'
  ic_filename     = 'ic_land_soil.nc'
/
&jsb_carbon_nml
  active                 = ${lcarbon}
  bc_filename            = 'bc_land_carbon.nc'
  ic_filename            = 'ic_land_carbon.nc'
  read_cpools            = .FALSE.
/
&jsb_fuel_nml
  active                 = ${lcarbon}
  fuel_algorithm         = 1
/
&jsb_disturb_nml
  active                  = .FALSE.
  ic_filename             = 'ic_land_soil.nc'
  bc_filename             = 'bc_land_phys.nc'
  fire_algorithm          = 1
  windbreak_algorithm     = 1
  lburn_pasture           = .FALSE.
/
EOF

#--------------------------------------------------------------------------------------------------

# (4) Define the input

# model files
#
add_link_file ${basedir}/data/rrtmgp-data-lw-g256-2018-12-04.nc         ./coefficients_lw.nc
add_link_file ${basedir}/data/rrtmgp-data-sw-g224-2018-12-04.nc         ./coefficients_sw.nc
add_link_file ${basedir}/data/ECHAM6_CldOptProps_rrtmgp_lw.nc           ./rrtmgp-cloud-optics-coeffs-lw.nc
add_link_file ${basedir}/data/ECHAM6_CldOptProps_rrtmgp_sw.nc           ./rrtmgp-cloud-optics-coeffs-sw.nc

# namelist files
#
add_required_file ${thisdir}/${atmo_namelist}                       ./
add_required_file ${thisdir}/${lnd_namelist}                        ./

# dictionary file for output variable names
#
dict_file=dict.iconam.mpim
add_required_file ${basedir}/run/${dict_file}                           ./

# atmosphere grid
#
datadir=${icon_data_rootFolder}/${atm_grid_id}
add_required_file ${datadir}/${atm_grid_file}                           ./

# initial conditions
#
# - atmosphere: ECMWF analysis for ${start_date}
inidate=${start_date%T*}  # remove time of day
inidate=${inidate//-}     # short format: yyyy-mm-dd -> yyyymmdd
inidate=${inidate}00      # add hour
iniyearmon=${inidate:0:6} # yyymm
inifile=echam2icon_${inidate}_${atm_grid_id}_${atm_grid_refinement}_${atm_grid_label}.nc
datadir=${icon_data_rootFolder}/${atm_grid_id}/initial_conditions/{{sst_scenario}}/
#datadir=${icon_data_rootFolder}/qubicc-initial-conditions-try.2/
add_link_file ${datadir}/${inifile}                                     ./ifs2icon.nc
#
# - land: source?, date+time?
datadir=${icon_data_rootFolder}/${atm_grid_id}/land/{{sst_scenario}}/
inifile=echam2icon_ic_land_soil_${iniyearmon}_${atm_grid_id}_${atm_grid_refinement}_${atm_grid_label}.nc
add_link_file ${datadir}/${inifile}                                     ./ic_land_soil.nc

# boundary conditions
#
# range of years for yearly files
# assume start_date and end_date have the format yyyy-...
start_year=$(( ${start_date%%-*} - 1 ))
end_year=$(( ${end_date%%-*} + 1 ))
#
# - well mixed greenhouse gases
datadir=${icon_data_rootFolder}/common/greenhouse_gases
add_link_file $datadir/greenhouse_{{rcp_scenario}}.nc                    ./bc_greenhouse_gases.nc
#
# - ozone
datadir=${icon_data_rootFolder}/${atm_grid_id}/ozone
year=$start_year
while [[ $year -le $end_year ]]
do
  if [[ $year -le 2009 ]]
  then
    yyyy=$year
  else
    yyyy=2009
  fi
  add_link_file $datadir/bc_ozone_historical_${yyyy}.nc                 ./bc_ozone_${year}.nc
  (( year = year+1 ))
done
#
# - ssi and tsi
datadir=${icon_data_rootFolder}/common/solar_radiation
add_link_file $datadir/swflux_14band_cmip6_1850-2299-v3.2.nc            ./bc_solar_irradiance_sw_b14.nc
#
# - sst and seasic
datadir=${icon_data_rootFolder}/${atm_grid_id}/sst_and_seaice/{{sst_scenario}}/
add_link_file $datadir/mpiesm1_bc_sst-sic_r{{ensemble_id}}_${atm_grid_id}_${atm_grid_refinement}_${atm_grid_label}.nc     ./sst-sic-runmean_G.nc
#
# - topography and sso parameters
datadir=${icon_data_rootFolder}/${atm_grid_id}/land
add_link_file $datadir/bc_land_sso_1992.nc                              ./bc_land_sso.nc
#
# - land parameters
datadir=${icon_data_rootFolder}/${atm_grid_id}/land
add_link_file $datadir/bc_land_frac_${pft_file_tag}1992.nc              ./bc_land_frac.nc
add_link_file $datadir/bc_land_phys_1992.nc                             ./bc_land_phys.nc
add_link_file $datadir/bc_land_soil_1992.nc                             ./bc_land_soil.nc
#
# - lctlib file for JSBACH
add_link_file ${basedir}/externals/jsbach/data/lctlib_nlct21.def        ./lctlib_nlct21.def

#--------------------------------------------------------------------------------------------------

# (5) Define the output
output_filetype=2

# Parameters for all output files
# -------------------------------
cat >> ${atmo_namelist} << EOF
&io_nml
 output_nml_dict  = "${dict_file}"
 netcdf_dict      = "${dict_file}"
 itype_pres_msl   = 4
 restart_file_type= 5
 restart_write_mode = "joint procs multifile"
/
EOF
 
# Define output files
# -------------------
 
# constants on all levels (output at start_date only)
# -----------------------
for var in zghalf zg dzghalf
do
    cat >> ${atmo_namelist} << EOF
&output_nml
 output_filename  = "${EXPNAME}_${var}"
 filename_format  = "<output_filename>_<levtype_l>"
 filetype         = ${output_filetype}
 remap            = 0
 output_grid      = .FALSE.
 output_start     = "${start_date}"
 output_end       = "${start_date}"
 output_interval  = "${atm_3d_output_interval}"
 file_interval    = "${atm_3d_file_interval}"
 ml_varlist       = '${var}'
/
EOF
done
 
 # time-averaged variables, 3-dimensional
 # --------------------------------------
for var in ps pfull ta tend_ta_mig rho ua va wa hus clw cli qr qs qg
do
    cat >> ${atmo_namelist} << EOF
&output_nml
 output_filename  = "${EXPNAME}_${var}"
 filename_format  = "<output_filename>_<levtype_l>_<datetime2>"
 filetype         = ${output_filetype}
 remap            = 0
{%- if atm_3d_output_operation != "none" %}
 operation        = "{{ atm_3d_output_operation }}"
{%- endif %}
 output_grid      = .FALSE.
 output_start     = "${output_start}"
 output_end       = "${output_end}"
 output_interval  = "${atm_3d_output_interval}"
 file_interval    = "${atm_3d_file_interval}"
 include_last     = .FALSE.
 m_levels         = "43...nlev"  ! Only write 3d variables up to htop_moist_proc
 ml_varlist       = '${var}'
/
EOF
done

# instantaneous variables, 2-dimensional
# --------------------------------------
cat >> ${atmo_namelist} << EOF
&output_nml
 output_filename  = "${EXPNAME}_atm2d"
 filename_format  = "<output_filename>_<levtype_l>_<datetime2>"
 filetype         = ${output_filetype}
 remap            = 0
 output_grid      = .FALSE.
 output_start     = "${output_start}"
 output_end       = "${output_end}"
 output_interval  = "${atm_2d_output_interval}"
 file_interval    = "${atm_2d_file_interval}"
 include_last     = .FALSE.
 ml_varlist       = 'cosmu0'  , 'albedo'  ,
                    'ps'      , 'ts'      ,
                    'sic'     , 'sit'     ,
                    'clt'     ,
                    'prw'     , 'cllvi'   , 'clivi'   , 'qrvi'    , 'qsvi'    , 'qgvi'    ,
                    'sfcwind' , 'uas'     , 'vas'     ,
                    'tas'     , 'dew2'    ,
                    'ptp'     , 'psl'
/
EOF

cat >> ${atmo_namelist} << EOF
&output_nml
 output_filename  = "${EXPNAME}_atm2d_500"
 filename_format  = "<output_filename>_<levtype_l>_<datetime2>"
 filetype         = ${output_filetype}
 remap            = 0
 output_grid      = .FALSE.
 output_start     = "${output_start}"
 output_end       = "${output_end}"
 output_interval  = "${atm_2d_output_interval}"
 file_interval    = "${atm_2d_file_interval}"
 include_last     = .FALSE.
 pl_varlist       = 'omega', 'zg'
 p_levels         = 50000
/
EOF

# time mean variables, 2-dimensional
# ----------------------------------
cat >> ${atmo_namelist} << EOF
&output_nml
 output_filename  = "${EXPNAME}_flx2d"
 filename_format  = "<output_filename>_<levtype_l>_<datetime2>"
 filetype         = ${output_filetype}
 remap            = 0
 operation        = 'mean'
 output_grid      = .FALSE.
 output_start     = "${output_start}"
 output_end       = "${output_end}"
 output_interval  = "${atm_2d_output_interval}"
 file_interval    = "${atm_2d_file_interval}"
 include_last     = .FALSE.
 ml_varlist       = 'rsdt'    ,
                    'rsut'    , 'rsutcs'  , 'rlut'    , 'rlutcs'  ,
                    'rsds'    , 'rsdscs'  , 'rlds'    , 'rldscs'  ,
                    'rsus'    , 'rsuscs'  , 'rlus'    ,
                    'rain_gsp_rate'       , 'snow_gsp_rate'       ,
                    'graupel_gsp_rate'    , 'evspsbl' ,
                    'hfls'    , 'hfss'    ,
                    'tauu'    , 'tauv'
/
EOF

#___________________________________________________________________________________________________________________________________
#
#_______________________________________________________________________________________________======================================================
#___________________________________________________________________________________________________________________________________
#
# This section of the run script prepares and starts the model integration.
#
# MODEL and START must be defined as environment variables or
# they must be substituted with appropriate values.
#
# Marco Giorgetta, MPI-M, 2010-04-21
#
#-----------------------------------------------------------------------------
#
# directories definition
#
RUNSCRIPTDIR=${thisdir}
if [ x$grids_folder = x ] ; then
   HGRIDDIR=${basedir}/grids
else
   HGRIDDIR=$grids_folder
fi

# experiment directory, with plenty of space, create if new
EXPDIR=/p/scratch/highresmonsoon/experiments/${EXPNAME}
if [ ! -d ${EXPDIR} ] ;  then
  mkdir -p ${EXPDIR}
fi
#
ls -ld ${EXPDIR}
if [ ! -d ${EXPDIR} ] ;  then
    mkdir ${EXPDIR}
#else
#   rm -rf ${EXPDIR}
#   mkdir  ${EXPDIR}
fi
ls -ld ${EXPDIR}
check_error $? "${EXPDIR} does not exist?"

cd ${EXPDIR}

#-----------------------------------------------------------------------------
final_status_file=${RUNSCRIPTDIR}/${job_name}.final_status
rm -f ${final_status_file}

#-----------------------------------------------------------------------------
# set up the model lists if they do not exist
# this works for single model runs
# for coupled runs the lists should be declared explicilty
if [ x$namelist_list = x ]; then
#  minrank_list=(        0           )
#  maxrank_list=(     65535          )
#  incrank_list=(        1           )
  minrank_list[0]=0
  maxrank_list[0]=65535
  incrank_list[0]=1
  if [ x$atmo_namelist != x ]; then
    # this is the atmo model
    namelist_list[0]="$atmo_namelist"
    modelname_list[0]="atmo"
    modeltype_list[0]=1
    run_atmo="true"
  elif [ x$ocean_namelist != x ]; then
    # this is the ocean model
    namelist_list[0]="$ocean_namelist"
    modelname_list[0]="ocean"
    modeltype_list[0]=2
  elif [ x$psrad_namelist != x ]; then
    # this is the psrad model
    namelist_list[0]="$psrad_namelist"
    modelname_list[0]="psrad"
    modeltype_list[0]=3
  elif [ x$hamocc_namelist != x ]; then
    # this is the hamocc model
    namelist_list[0]="$hamocc_namelist"
    modelname_list[0]="hamocc"
    modeltype_list[0]=4
  elif [ x$testbed_namelist != x ]; then
    # this is the testbed model
    namelist_list[0]="$testbed_namelist"
    modelname_list[0]="testbed"
    modeltype_list[0]=99
  else
    check_error 1 "No namelist is defined"
  fi
fi

#-----------------------------------------------------------------------------


#-----------------------------------------------------------------------------
# set some default values and derive some run parameteres
restart=".false."
restartSemaphoreFilename='isRestartRun.sem'
#AUTOMATIC_RESTART_SETUP:
if [ -f ${restartSemaphoreFilename} ]; then
  restart=.true.
  #  do not delete switch-file, to enable restart after unintended abort
  #[[ -f ${restartSemaphoreFilename} ]] && rm ${restartSemaphoreFilename}
fi
#END AUTOMATIC_RESTART_SETUP
#
# wait 5min to let GPFS finish the write operations
if [ "x$restart" != 'x.false.' -a "x$submit" != 'x' ]; then
  if [ x$(df -T ${EXPDIR} | cut -d ' ' -f 2) = gpfs ]; then
    sleep 10;
  fi
fi
# fill some checks

run_atmo=${run_atmo="false"}
if [ x$atmo_namelist != x ]; then
  run_atmo="true"
fi
run_jsbach=${run_jsbach="false"}
run_ocean=${run_ocean="false"}
if [ x$ocean_namelist != x ]; then
  run_ocean="true"
fi
run_psrad=${run_psrad="false"}
if [ x$psrad_namelist != x ]; then
  run_psrad="true"
fi
run_hamocc=${run_hamocc="false"}
if [ x$hamocc_namelist != x ]; then
  run_hamocc="true"
fi

#-----------------------------------------------------------------------------
# add grids to required files
all_grids="${atmo_dyn_grids} ${atmo_rad_grids} ${ocean_grids}"
for gridfile in ${all_grids}; do
  #
  gridfile=${gridfile//\'/} # strip all ' in case ' is used to delimit the grid names
  gridfile=${gridfile//\"/} # strip all " in case " is used to delimit the grid names
  gridfile=${gridfile//\,/} # strip all , in case , is used to separate the grid names
  #
  grfinfofile=${gridfile%.nc}-grfinfo.nc
  #
  ls -l ${HGRIDDIR}/$gridfile
  check_error $? "${HGRIDDIR}/$gridfile does not exist."
  # copy gridfile: can be very expensive
  # add_required_file ${HGRIDDIR}/${gridfile} ./
  # replace by linking
  add_link_file ${HGRIDDIR}/${gridfile} ./
  if [ -f ${HGRIDDIR}/${grfinfofile} ]; then
      # same here
      # add_required_file ${HGRIDDIR}/${grfinfofile} ./
      add_link_file ${HGRIDDIR}/${grfinfofile} ./
  fi
done
#-----------------------------------------------------------------------------
# print_required_files
copy_required_files
link_required_files


#-----------------------------------------------------------------------------
# get restart files

if  [ x$restart_atmo_from != "x" ] ; then
  rm -f restart_atm_DOM01.nc
#  ln -s ${basedir}/experiments/${restart_from_folder}/${restart_atmo_from} ${EXPDIR}/restart_atm_DOM01.nc
  cp ${basedir}/experiments/${restart_from_folder}/${restart_atmo_from} cp_restart_atm.nc
  ln -s cp_restart_atm.nc restart_atm_DOM01.nc
  restart=".true."
fi
if  [ x$restart_ocean_from != "x" ] ; then
  rm -f restart_oce.nc
#  ln -s ${basedir}/experiments/${restart_from_folder}/${restart_ocean_from} ${EXPDIR}/restart_oce.nc
  cp ${basedir}/experiments/${restart_from_folder}/${restart_ocean_from} cp_restart_oce_DOM01.nc
  ln -s cp_restart_oce_DOM01.nc restart_oce_DOM01.nc
  restart=".true."
fi
#-----------------------------------------------------------------------------

read_restart_namelists=${read_restart_namelists:=".true."}

#-----------------------------------------------------------------------------
#
# create ICON master namelist
# ------------------------
# For a complete list see Namelist_overview and Namelist_overview.pdf

#-----------------------------------------------------------------------------
# create master_namelist
master_namelist=icon_master.namelist
if [ x$end_date = x ]; then
cat > $master_namelist << EOF
&master_nml
 lrestart            = $restart
/
&master_time_control_nml
 experimentStartDate  = "$start_date"
 restartTimeIntval    = "$restart_interval"
 checkpointTimeIntval = "$checkpoint_interval"
/
&time_nml
 is_relative_time = .false.
/
EOF
else
if [ x$calendar = x ]; then
  calendar='proleptic gregorian'
  calendar_type=1
else
  calendar=$calendar
  calendar_type=$calendar_type
fi
cat > $master_namelist << EOF
&master_nml
 lrestart            = $restart
 read_restart_namelists = $read_restart_namelists
/
&master_time_control_nml
 calendar             = "$calendar"
 checkpointTimeIntval = "$checkpoint_interval"
 restartTimeIntval    = "$restart_interval"
 experimentStartDate  = "$start_date"
 experimentStopDate   = "$end_date"
/
&time_nml
 is_relative_time = .false.
/
EOF
fi
#-----------------------------------------------------------------------------


#-----------------------------------------------------------------------------
# add model component to master_namelist
add_component_to_master_namelist()
{

  model_namelist_filename="$1"
  model_name=$2
  model_type=$3
  model_min_rank=$4
  model_max_rank=$5
  model_inc_rank=$6

cat >> $master_namelist << EOF
&master_model_nml
  model_name="$model_name"
  model_namelist_filename="$model_namelist_filename"
  model_type=$model_type
  model_min_rank=$model_min_rank
  model_max_rank=$model_max_rank
  model_inc_rank=$model_inc_rank
/
EOF

#-----------
#get namelist
  if [ -f ${RUNSCRIPTDIR}/$model_namelist_filename ] ; then
    mv -f ${RUNSCRIPTDIR}/$model_namelist_filename ${EXPDIR}
    check_error $? "mv -f ${RUNSCRIPTDIR}/$model_namelist_filename"
  else
    check_error 1 "${RUNSCRIPTDIR}/$model_namelist_filename does not exist"
  fi

}
#-----------------------------------------------------------------------------


{% raw %}no_of_models=${#namelist_list[*]}{% endraw %}
echo "no_of_models=$no_of_models"

j=0
while [ $j -lt ${no_of_models} ]
do
  add_component_to_master_namelist "${namelist_list[$j]}" "${modelname_list[$j]}" ${modeltype_list[$j]} ${minrank_list[$j]} ${maxrank_list[$j]} ${incrank_list[$j]}
  j=`expr ${j} + 1`
done

#-----------------------------------------------------------------------------
# Add JSBACH part to master_namelist

if [[ $run_jsbach == @(yes|true) ]]; then
  cat >> $master_namelist << EOF
&jsb_control_nml
 is_standalone      = .false.
 restart_jsbach     = .false.
 debug_level        = 0
 timer_level        = 0
/
EOF
#
if [[ -n ${atmo_dyn_grids} ]]; then
  set -A gridfiles $atmo_dyn_grids
  {% raw %}no_of_domains=${#gridfiles[*]}{% endraw %}
else
  no_of_domains=1
fi
echo "no_of_domains=$no_of_domains"
domain=""
domain_suffix=""
j=1
while [ $j -le ${no_of_domains} ]
do
  if [[ $no_of_domains -gt 1 ]]; then
    # no_of_domains < 10 !
    domain=" DOM0${j}"
    domain_suffix="_d${j}"
  fi
  cat >> $master_namelist << EOF
&jsb_model_nml
 model_id = $j
 model_name = "JSBACH${domain}"
 model_shortname = "jsb${domain_suffix}"
 model_description = 'JSBACH land surface model'
 model_namelist_filename = "${lnd_namelist}${domain_suffix}"
/
EOF
  if [[ -f ${RUNSCRIPTDIR}/NAMELIST_${EXPNAME}_lnd${domain_suffix} && -f ${EXPDIR}/NAMELIST_${EXPNAME}_lnd${domain_suffix} ]] ; then
    # namelist file has already been copied to expdir by copy_required_files above
    rm ${RUNSCRIPTDIR}/NAMELIST_${EXPNAME}_lnd${domain_suffix}
    check_error $? "rm ${RUNSCRIPTDIR}/NAMELIST_${EXPNAME}_lnd${domain_suffix}"
  else
    check_error 1 "${RUNSCRIPTDIR}/NAMELIST_${EXPNAME}_lnd${domain_suffix} does not exist"
  fi
  j=`expr ${j} + 1`
done
fi
#
#  get model
#
ls -l ${MODEL}
check_error $? "${MODEL} does not exist?"
#
ldd -v ${MODEL}
#
printenv | grep SLURM
#-----------------------------------------------------------------------------
#
# Prepare nodelist
#export SLURM_HOSTFILE=hostfile
#${basedir}/run/create_hostfile.sh $mpi_procs_pernode $mpi_io_procs $SLURM_HOSTFILE

#
# start experiment
#
rm -f finish.status
#
date
${START} ${MODEL} # > out.txt 2>&1

date
#
if [ -r finish.status ] ; then
  check_final_status 0 "${START} ${MODEL}"
else
  check_final_status -1 "${START} ${MODEL}"
fi
#
#-----------------------------------------------------------------------------
#
finish_status=`cat finish.status`
echo $finish_status
echo "============================"
echo "Script run successfully: $finish_status"
echo "============================"
#-----------------------------------------------------------------------------
if [[ "x$use_hamocc" = "xyes" ]]; then
# store HAMOCC log file
strg="$(ls -rt ${EXPNAME}_hamocc_EU*.nc* | tail -1 )"
prefx="${EXPNAME}_hamocc_EU_tendencies"
foo=${strg##${prefx}}
foo=${foo%%.*}
bgcout_file="bgcout_${foo}"
mv bgcout $bgcout_file
fi
#-----------------------------------------------------------------------------
namelist_list=""
#-----------------------------------------------------------------------------
# check if we have to restart, ie resubmit
#   Note: this is a different mechanism from checking the restart
if [ $finish_status = "RESTART" ] ; then
  echo "restart next experiment..."
  this_script="${RUNSCRIPTDIR}/${job_name}"
  echo 'this_script: ' "$this_script"
  touch ${restartSemaphoreFilename}
  cd ${RUNSCRIPTDIR}
  # Submission of consecutive jobs is handled via a wrapper shell script
  #off# ${submit} $this_script
else
  [[ -f ${restartSemaphoreFilename} ]] && rm ${restartSemaphoreFilename}
fi

#-----------------------------------------------------------------------------
# automatic call/submission of post processing if available
if [ "x${autoPostProcessing}" = "xtrue" ]; then
  # check if there is a postprocessing is available
  cd ${RUNSCRIPTDIR}
  targetPostProcessingScript="./post.${EXPNAME}.run"
  #off# [[ -x $targetPostProcessingScript ]] && ${submit} ${targetPostProcessingScript}
  cd -
fi

#-----------------------------------------------------------------------------

cd $RUNSCRIPTDIR

#-----------------------------------------------------------------------------


# exit 0
#
# vim:ft=sh
#-----------------------------------------------------------------------------
