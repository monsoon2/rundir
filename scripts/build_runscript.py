#!/usr/bin/env python3
"""Generate ICON run script from configuration YAML.
"""
import argparse
from pathlib import Path

import jinja2
import yaml


def build_runscript(config_yaml):
    # Read run settings from YAML.
    config_yaml = Path(config_yaml)
    with config_yaml.open("r") as f:
        config = yaml.safe_load(f)

    # Load Jinja2 template based on ICON resolution.
    file_loader = jinja2.FileSystemLoader("./templates")
    env = jinja2.Environment(loader=file_loader)
    template = env.get_template(config["template"])

    # Render and store runsript.
    runid = config["experiment_id"]
    runscript = config_yaml.parent / f"exp.{runid}.run"

    with runscript.open("w") as f:
        f.write(template.render(**config))


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="Generate ICON run script.")
    parser.add_argument(
        "config_yaml", type=str, help="YAML file containing run settings"
    )
    args = parser.parse_args()

    build_runscript(args.config_yaml)
