#!/usr/bin/env python3
"""Generate ICON run scripts for the Monsoon 2.0 project.
"""
import argparse
import itertools
import warnings
from pathlib import Path

import jinja2
import yaml


def create_rundir(experiment_id):
    """Create a run directory and link common auxiliary scripts."""
    rundir = Path(experiment_id)

    rundir.mkdir(exist_ok=True)

    for file in Path("common").iterdir():
        source = Path("..", file)
        target = Path(rundir, file.name)
        if not target.exists():
            target.symlink_to(source)

    return rundir


def create_config(experiment_id):
    """Create a run script for a given experiment ID."""
    # Determine ICON configuration based on experiment ID.
    config = get_configuration(experiment_id)

    # Create rundir and configuration YAML.
    rundir = create_rundir(experiment_id)

    config_yaml = Path(rundir, f"exp.{experiment_id}.yaml")
    with open(config_yaml, mode="w") as f:
        yaml.dump(config, f)


def get_configuration(experiment_id):
    """Return the ICON configuration based on a given experiment ID.

        Experiment ID: tagIJKL

            tag: Three-letter name tag for the run
            I, J, K, L: Digits to identify a specific run
    """
    config = dict(experiment_id=experiment_id)

    user = experiment_id[:3]  # tag
    batch_id = experiment_id[3]  # I
    echam_mig = experiment_id[4]  # J
    scenario = experiment_id[5]  # K
    ensemble = experiment_id[6]  # L

    config["ensemble_id"] = int(ensemble)
    config["rcp_scenario"] = "ssp245"

    # R02B04 debug runs (based on ensemble)
    if batch_id == "0":
        config["template"] = "r2b4.tmpl"
        config["atm_grid_refinement"] = "R02B04"

    # R02B09 aka "Monsoon ensemble"
    if batch_id == "1":
        if int(ensemble) <= 3:
            config["template"] = f"ensemble-1.{ensemble}.tmpl"
            config["atm_3d_output_interval"] = "P7D"
            config["atm_3d_output_operation"] = "mean"
        else:
            config["template"] = "ensemble-1.4.tmpl"
            config["atm_3d_output_interval"] = "PT3H"
            config["atm_3d_output_operation"] = "none"

        config["atm_grid_refinement"] = "R02B09"
        config["icon_basedir"] = "/p/project/highresmonsoon/ICON/icon-cscs-qubicc3-monsoon2.0"

        if scenario == "0":
            config["sst_scenario"] = "piControl"
            year = "2020"
        elif scenario == "1":
            config["sst_scenario"] = "ssp245"
            year = "2070"
        elif scenario == "2":
            config["sst_scenario"] = "ssp585"
            config["rcp_scenario"] = "ssp585"
            year = "2070"
        elif scenario == "3":
            config["sst_scenario"] = "piControl-p1K"
            year = "2020"
        else:
            raise ValueError(f"Undefined major experiment indentifier: {scenario}")

        config["start_date"] = f"{year}-04-01T00:00:00Z"
        config["end_date"] = f"{year}-10-01T00:00:00Z"

        # Increase fall speed of ice particles in microphysics scheme.
        if echam_mig == "1":
            config["zvz0i"] = 3.29
            config["icesedi_exp"] = 0.4
        else:
            config["zvz0i"] = 1.25
            config["icesedi_exp"] = 0.33

    # R2B10 simulations aka "Case studies"
    if batch_id == "2":
        config["template"] = "casestudy.tmpl"
        config["icon_basedir"] = "/p/project/highresmonsoon/ICON/icon-cscs-2.6.5-sst"

        if ensemble == "0":
            config["atm_3d_output_interval"] = "P1D"
            config["atm_3d_output_operation"] = "mean"
        else:
            config["atm_3d_output_interval"] = "PT3H"
            config["atm_3d_output_operation"] = "none"

        if ensemble in ["0", "1"]:
            config["atm_grid_refinement"] = "R02B10"
            config["atm_grid_id"] = "0039"
            config["model_timestep"] = "PT20S"
            config["nodes"] = 308
        elif ensemble in ["2", "3"]:
            config["atm_grid_refinement"] = "R02B09"
            config["atm_grid_id"] = "0015"
            config["model_timestep"] = "PT40S"
            config["nodes"] = 76

        if ensemble in ["0", "2"]:
            config["start_date"] = "2021-07-12T00:00:00Z"
            config["end_date"] = "2021-08-01T00:00:00Z"
        elif ensemble in ["1", "3"]:
            config["start_date"] = "2021-07-18T00:00:00Z"
            config["end_date"] = "2021-08-07T00:00:00Z"

    return config


def get_monsoon2_experiment_ids():
    """Helper function to create all experiment IDs for the Monsoon 2 project."""
    name = "luk"
    i = 1
    j = 0
    minor = (0, 1, 2, 3)
    ens = range(8)

    return [f"{name}{i}{j}{k}{l}" for k, l in itertools.product(minor, ens)]


def _main(experiment_ids=None):
    for exp_id in experiment_ids:
        create_config(exp_id)

        import argparse


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="Generate ICON run scripts.")
    parser.add_argument(
        "experiment_ids",
        metavar="tagNNNN",
        type=str,
        nargs="*",
        help="list of experiment IDs",
        default=get_monsoon2_experiment_ids(),
    )
    args = parser.parse_args()

    _main(args.experiment_ids)
